# ➕ ➖ Infix Calculator ✖️ ➗

An implementation of a calculator with operator precedence for Arduino.

It is based on the [Shunting-yard algorithm](https://en.wikipedia.org/wiki/Shunting-yard_algorithm) in order to convert from [infix notation](https://en.wikipedia.org/wiki/Infix_notation) (eg `1+2*3`) to [postfix notation also known as Reverse Polish notation (RPN)](https://en.wikipedia.org/wiki/Reverse_Polish_notation) (eg `1 2 3 * +`) in order to calculate the result of a given expression.

This calculator supports:
- `+`, `-`, `*`, and `/` operators with `*`, and `/` taking precedence
- `(` and `)`

This project is a fork of [efstathios-chatzikyriakidis/infix-calculator-lcd-keypad-shield-serial](https://github.com/efstathios-chatzikyriakidis/infix-calculator-lcd-keypad-shield-serial) with the following modifications:
- removed the hardware specific code (LCD and keypad) in order to be hardware agnostic;
- allow the calculator to be embedded in a project;
- allow input of a decimal number;
- ported in `C` to ease the inclusion in external project.

## ⚠️ Limitations ⚠️

⚠️ The ATmega cannot handle "big number": according to [Arduino's documentation](https://www.arduino.cc/reference/en/language/variables/data-types/double/) `double` acts as `float` and thus are limited to 6-7 decimal digits of precision.
For instance, the result of the operation `12345679*9` is `111111111` but it is rounded to `111111110`.

One must not use this kind of processor for high precision computation.

## Usage

To include this calculator in an Arduino project:
- copy all `.cpp` and `.h` files of this repository into your sketch folder;
- add the include before the first instruction:
```c
#include "calculator.h"
```
- call the `calculate(char *infix, double & numerical_result)` function with the following arguments:
  - `infix`: expression to evaluate, e.g. `1+2*3`;
  - `numerical_result`: variable to store the result;
  - it will return an error if something went wrong, see [`calculator.h`](calculator.h) for the exact list.

### Example
```c
#include "calculator.h"

void setup() {
  Serial.begin (9600);
  // the error message
  char error[64];
  // the result
  double result = 0;
  bool status = calculate("1+2*3", result, error);
  if (status) {
    // success
    char output[64];
    dtostrf(result, 1, 2, output);
    Serial.println(output);
  } else {
    // an error occured during the calcul
    Serial.println(error);
  }
}

void loop() {}

```

With `1+2*3`, it will output:
```
7.00
```

An invalid expression like `1 2` will return the error code `7` which refers to `ERR_EXPR_TOO_MANY_VALUES`.

With a decimal input, `2.6+9` will result in `11.60`.

### Provided sketch

The sketch provided in `infix_calculator.ino` listens for inputs on the serial console and displays the result.

### Compilation and upload

To build and upload:
```
killall screen; arduino-cli compile --fqbn arduino:avr:leonardo  -u -p /dev/cu.usbmodem14401
```

To monitor the serial:
```
screen /dev/cu.usbmodem14401
```

## Memory usage

It takes about 6000 bytes of flash and 420 bytes of RAM.
I compared it with an empty sketch with the `Serial` enabled.

## Links

- [Source this work is based on](https://efxa.org/2010/09/18/arduino_infix_calculator)
- [Calculator icon by Icons8](https://icons8.com/icons/set/calculator--v1)
