/*
 *  This file implements a postfix evaluation algorithm.
 *
 *  Copyright (C) 2021 Coliss86
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <math.h>
#include "evaluate_postfix.h"
#include "string_utils.h"
#include "calculator.h"

#define STACK_SIZE 20

double postfix_stack[STACK_SIZE]; // the postfix stack.
int    postfix_stack_top = -1;    // stack top position

// postfix expression evaluation algorithm.
uint8_t evaluate_postfix(char *postfix, double & numerical_result) {

  char c; // the character parsed.

  postfix_reset();

  // check if the expression is empty.
  if (strlen(postfix) <= 0) {
    return ERR_EXPR_EMPTY;
  }

  // handle each character from the postfix expression.
  for (int i = 0; postfix[i] != '\0'; i++) {
    // get the character.
    c = postfix[i];

    // if the character is not white space.
    if (c != ' ' && c != '\t') {
      // if the character is an identifier.
      if (is_numeral(c)) {
        // necessary for later reference.
        if (!postfix_push(0)) {
          return ERR_STACKOVERFLOW;
        }
        int decimal = 0;
        // try to fetch / calculate a multi-digit integer number.
        for (; postfix[i] != '\0' && is_numeral_or_decimal(c = postfix[i]); i++) {
          // calculate the number so far with its digits.
          if (c == '.') {
            decimal = 1;
            continue;
          }

          if (decimal > 0) {
            if (!postfix_push(postfix_pop() + (c - '0') / pow(10, decimal++))) {
              return ERR_STACKOVERFLOW;
            }
          }
          else {
            if (!postfix_push(10.0 * postfix_pop() + (c - '0'))) {
              return ERR_STACKOVERFLOW;
            }
          }
        }

        // fix the index in order to 'ungetch' the non-identifier character.
        i--;
      }
      // if the character is an operator.
      else if (is_operator(c)) {
        // get the number of operator's arguments.
        int nargs = op_operands_count(c);

        // if there aren't enough arguments in the stack.
        if (nargs > postfix_count()) {
          return ERR_INSUFFICIENT_OPERATOR;
        }

        // allocate enough memory for the arguments of the operator.
        double *vargs = (double *) malloc(sizeof(double) * nargs);

        // if there was memory allocation error.
        if (vargs == NULL) {
          return ERR_MEMORY;
        }

        // fetch all the arguments of the operator.
        for (int arg = 0; arg < nargs; arg++)
          vargs[arg] = postfix_pop();

        // evaluate the operator with its operands.
        switch(c) {
          case '+':
            if (!postfix_push(vargs[1] + vargs[0])) {
              return ERR_STACKOVERFLOW;
            }
            break;

          case '-':
            if (!postfix_push(vargs[1] - vargs[0])) {
              return ERR_STACKOVERFLOW;
            }
            break;

          case '/':
            if (vargs[0] != 0) {
              if (!postfix_push((double)(vargs[1] / vargs[0]))) {
                return ERR_STACKOVERFLOW;
              }
            } else {
              return ERR_DIVISION_BY_0;
            }
            break;

          case '*':
            if (!postfix_push(vargs[1] * vargs[0])) {
              return ERR_STACKOVERFLOW;
            }
            break;
        }

        // deallocate memory for operands.
        free(vargs);
      }
      // the character is unknown.
      else {
        return ERR_UNKNOW_CHAR;
      }
    }
  }

  // if there is only one element in the stack.
  if (postfix_count() == 1) {
    // return the result of the expression.
    numerical_result = postfix_pop();
  }
  else {
    return ERR_EXPR_TOO_MANY_VALUES;
  }

// On ATmega, the precision is not accurate
#if defined(ARDUINO_ARCH_AVR)
  if (numerical_result > 1000000 || numerical_result < -1000000) {
    return ERR_RESULT_TOO_HIGH;
  }
#endif

  // postfix evaluation done.
  return NO_ERROR;
}

bool postfix_push(double ch) {
  if (postfix_stack_top >= STACK_SIZE) {
    return false;
  }
  postfix_stack_top = postfix_stack_top + 1;
  postfix_stack[postfix_stack_top] = ch;
  return true;
}

double postfix_peek() {
  return postfix_stack[postfix_stack_top];
}

double postfix_pop() {
  double item = postfix_stack[postfix_stack_top];
  postfix_stack[postfix_stack_top] = NULL;
  postfix_stack_top = postfix_stack_top - 1;
  return item;
}

bool postfix_empty() {
  return postfix_stack_top == -1;
}

int postfix_count() {
  return postfix_stack_top + 1;
}

void postfix_reset() {
  postfix_stack_top = -1;
  postfix_stack[0] = 0;
}
