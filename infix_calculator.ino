/*
 *  Infix Calculator.
 *
 *  Copyright (C) 2021 Coliss86
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "calculator.h"
#include "string_utils.h"

char* ERR_MSG[] = { \
  "", \
  "Expr too long",  \
  "Expr empty", \
  "Not sufficient operator arguments", \
  "Insufficient memory", \
  "Division by zero", \
  "Unknown token", \
  "Too many values", \
  "Result too high", \
  "Stack overflow", \
  "Parentheses mismatched"
};

// startup point entry (runs once).
void setup() {
  // set serial data rate.
  Serial.begin(9600);

  //------------------------------
  delay(5000);
  Serial.println("gooooo");
  test_calculate("2+2");
  test_calculate("1+2*3");
  test_calculate("1.2+3");
  test_calculate("32 + 111 / ( 4 - 43 ) * 33 / ( 2 + 3 ) + (( 3 - 2 ) / ( 2 + 1 ))");
  test_calculate("2 2");
  test_calculate("1/3");
  test_calculate("200*569887");
  Serial.println("-------------------------");
}

// loop the main sketch.
void loop() {
  String infix;                // the infix expression.
  double result = 0;
  char output[RESULT_MAX_SIZE];
  bool status;

  // try to read an expression.
  if (read_line(infix)) {
    uint8_t status = calculate(infix.c_str(), result);
    if (status == 0) {
      dtostrf(result, 1, 2, output);
      Serial.println(output);
    } else {
      Serial.println(ERR_MSG[status]);
    }
  }
}

void test_calculate(String infix) {
  double result = 0;
  char output[RESULT_MAX_SIZE];
  Serial.print("Expression: ");
  Serial.println(infix);
  uint8_t status = calculate(infix.c_str(), result);
  if (status == 0) {
    dtostrf(result, 1, 2, output);
    Serial.println(output);
  } else {
    Serial.println(ERR_MSG[status]);
  }
  return true;
}

// read the serial. block until \r or \n is found
bool read_line(String & str) {
  str = "";
  char inByte = 0;

  while (inByte != '\n' && inByte != '\r') {

    while (!Serial.available());

    while (Serial.available() > 0) {
      // read the incoming byte:
      inByte = Serial.read();

      if (inByte == '\n' || inByte == '\r') {
        Serial.println(inByte);
        break;
      } else {
        Serial.print(inByte);
      }
      str += inByte;
    }
  }
  return true;
}
