/*
 *  This file implements the algorithm for infix->postfix.
 *
 *  Copyright (C) 2021 Coliss86
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "string_utils.h"
#include "infix_postfix.h"
#include "calculator.h"

#define STACK_SIZE 20

char operator_stack[STACK_SIZE]; // the operators stack.
int  operator_stack_top = -1;     // stack top position

// infix expression conversion to postfix(shunting-yard algorithm).
uint8_t infix_postfix(char *infix, char *postfix) {

  char c, sc; // the characters parsed.

  // check if the expression is empty.
  if (strlen(infix) <= 0) {
    return ERR_EXPR_EMPTY;
  }

  operator_reset();

  // handle each character from the infix expression.
  for (int i = 0; infix[i] != '\0'; i++) {
    // get the character.
    c = infix[i];

    // if the character is not white space.
    if (c != ' ' && c != '\t' && c != '\n' && c != '\r') {
      // if the character is an identifier.
      if (is_numeral(c)) {
        // add the character to postfix.
        append(postfix, c);

        // try to fetch more digits for multi-digit integer number and add them to postfix.
        for (++i; i < infix[i] != '\0' && is_numeral_or_decimal(c = infix[i]); i++) {
          if (c==',')
            append(postfix, '.');
          else
            append(postfix, c);
        }

        // fix the index in order to 'ungetch' the non-identifier character.
        i--;

        // add a space separator.
        append(postfix, ' ');
      }
      // if the character is an operator.
      else if (is_operator(c)) {
        // while the stack is not empty.
        while (!operator_empty()) {
          // get the top of the stack.
          sc = operator_peek();

          // if the top of the stack is an operator, check the
          // precedence and associativity of the two operators.
          if (is_operator(sc) &&
             ((op_left_associative(c) &&(op_precedence(c) <= op_precedence(sc))) ||
             (!op_left_associative(c) &&(op_precedence(c) < op_precedence(sc))))) {
            // add the appropriate operator to postfix.
            append(postfix, sc);

            // add a space separator.
            append(postfix, ' ');

            // pop the operator from the stack.
            operator_pop();
          }
          else
            break; // non-operator found.
        }

        // push the operator to the stack.
        if (!operator_push(c)) {
          return ERR_STACKOVERFLOW;
        }
      }
      // if the character is a left parenthesis.
      else if (c == '(') {
        // push it to the stack.
        if (!operator_push(c)) {
          return ERR_STACKOVERFLOW;
        }
      }
      // if the character is a right parenthesis.
      else if (c == ')') {
        bool pe = false;

        // until the token at the top of the stack
        // is left parenthesis, pop operators off
        // the stack onto the postfix expression.
        while (!operator_empty()) {
          sc = operator_peek();

          if (sc == '(') {
            pe = true;
            break;
          }
          else {
            // add the character to postfix.
            append(postfix, sc);

            // add a space separator.
            append(postfix, ' ');

            // pop the character from the stack.
            operator_pop();
          }
        }

        // if the stack runs out without finding a left
        // parenthesis there are mismatched parentheses.
        if (!pe) {
          return ERR_PARENTHESIS;
        }

        // pop the left parenthesis from the stack.
        operator_pop();
      }
    }
  }

  // when there are no more tokens to read: while
  // there are still operator tokens in the stack.
  while (!operator_empty()) {
    sc = operator_peek();

    if (sc == '(' || sc == ')') {
      return ERR_PARENTHESIS;
    }

    // add the character to postfix.
    append(postfix, sc);

    // add a space separator.
    append(postfix, ' ');

    // pop the character from the stack.
    operator_pop();
  }

  // infix->postfix parsing done.
  return NO_ERROR;
}

bool operator_push(char ch) {
  if (operator_stack_top >= STACK_SIZE) {
    return false;
  }
  operator_stack_top = operator_stack_top + 1;
  operator_stack[operator_stack_top] = ch;
  return true;
}

char operator_peek() {
  return operator_stack[operator_stack_top];
}

char operator_pop() {
  char item = operator_stack[operator_stack_top];
  operator_stack[operator_stack_top] = NULL;
  operator_stack_top = operator_stack_top - 1;
  return item;
}

bool operator_empty() {
  return operator_stack_top == -1;
}

void operator_reset() {
  operator_stack_top = -1;
  operator_stack[0] = 0;
}
