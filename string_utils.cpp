/*
 *  This file implements some parsing tools related to operators.
 *
 *  Copyright (C) 2021 Coliss86
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// include header defining the interface of the library.
#include "string_utils.h"

// get the precedence of an operator symbol.
//
// precedence   operators
// 1            !
// 2            * /
// 3            + -
int op_precedence(const char c) {
  switch (c) {
    case '*':
    case '/':
      return 3;

    case '+':
    case '-':
      return 2;
  }

  return 0;
}

// get the associativity of an operator symbol.
//
// associativity   operators
// right to left   !
// left to right   * /
// left to right   + -
bool
op_left_associative(const char c) {
  switch (c) {
    case '*':
    case '/':
      return true;

    case '+':
    case '-':
      return true;
  }

  return false;
}

// check if a character is an operator symbol.
bool is_operator (const char c) {
  switch (c) {
    case '/':
    case '*':

    case '+':
    case '-':
      return true;
  }

  return false;
}

// check if a character is a numeral symbol.
bool is_numeral(const char c) {
  if (c >= '0' && c <= '9')
    return true;

  return false;
}

// check if a character is a numeral symbol or a decimal separator.
bool is_numeral_or_decimal(const char c) {
  if (c >= '0' && c <= '9' || c == '.' || c == ',')
    return true;

  return false;
}

// get the number of the operands of an operator symbol.
int op_operands_count(const char c) {
  switch (c) {
    case '*':
    case '/':
    case '+':
    case '-':
      return 2;

    case '!':
      return 1;
  }

  return 0;
}

void append(char* s, char c) {
  int len = strlen(s);
  s[len] = c;
  s[len+1] = '\0';
}
