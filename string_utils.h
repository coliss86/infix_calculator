/*
 *  This file defines some parsing tools related to operators.
 *
 *  Copyright (C) 2021 Coliss86
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <string.h>

// get the number of the operands of an operator symbol.
int op_operands_count(const char c);

// get the precedence of an operator symbol.
int op_precedence(const char c);

// get the associativity of an operator symbol.
bool op_left_associative(const char c);

// check if a character is a numeral symbol.
bool is_numeral(const char c);

// check if a character is a numeral symbol or a decimal separator.
bool is_numeral_or_decimal(const char c);

// check if a character is an operator symbol.
bool is_operator(const char c);

// append a char to the char array
void append(char* s, char c);
